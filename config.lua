--[[
	[slot_id] = config:
	- {effect_id, custom_duration} = timer will start when the effect will fire
	- true = start timer instantly using duration from the ability description
	- false = ignore this slot
	- number = same as "true", but use value as a duration
]]
AltActionBar.abilityConfig = {
	-- Two Handed
	[28297] = {61665}, -- momentum (major brutality)
	[38794] = {61665}, -- forward momentum (major brutality)
	[38745] = {38747}, -- carve bleed

	-- Shield
	[28306] = {38541}, -- puncture (taunt)
	[38250] = {38541}, -- pierce armor (taunt)
	[38256] = {38541}, -- ransack (taunt)
	[28304] = 12, -- low slash (minor maim)
	[38268] = 12, -- deep slash (minor maim)
	[38264] = {61708}, -- heroic slash (minor heroism)

	-- Destruction Staff
	[29073] = {62648}, -- flame touch
	[29078] = {62692}, -- frost touch
	[29089] = {62722}, -- shock touch
	[38944] = {62682}, -- flame reach
	[38970] = {62712}, -- frost reach
	[38978] = {62745}, -- shock reach
	[38993] = {140334}, -- shock clench master staff
	[38985] = {140334}, -- flame clench master staff
	[38989] = {38541}, -- frost clench (taunt)
	[29173] = true, -- weakness to elements (major breach)
	[39089] = true, -- elemental susceptibility (major breach)
	[39095] = true, -- elemental drain (minor magickasteal)

	-- Restoration Staff
	[31531] = true, -- force siphon
	[40109] = true, -- siphon spirit
	[40116] = true, -- quick siphon
	[37243] = {61693}, -- blessing of protection (minor resolve)
	[40094] = {61744}, -- combat prayer (minor berserk)
	[40103] = {61693}, -- blessing of restoration (minor resolve)

	-- Dual Wield
	[21157] = {61665}, -- hidden blade (major brutality)
	[38914] = {61665}, -- shrouded daggers (major brutality)
	[38910] = {126667}, -- flying blade first cast
	[126659] = {61665}, -- flying blade jump
	[28379] = true,--{29293}, -- twin slashes
	[38839] = true,--{38841}, -- rending slashes
	[38845] = true,--{38848}, -- blood craze

	-- Bow
	[38687] = {38688}, -- focused aim (minor fracture)
	[28876] = {}, -- volley
	[38689] = {}, -- endless hail
	[38695] = {}, -- arrow barrage
	[38701] = true,--{38703}, -- acid spray
	[28869] = 10.5,--{44650}, -- poison arrow
	[38645] = 10.5,--{44545}, -- venom arrow
	[38660] = 10.5,--{44549}, -- poison injection

	-- Armor
	[29556] = true,--{63015}, -- evasion
	[39192] = true,--{63030}, -- elude
	[39195] = true,--{63019}, -- shuffle
	[29552] = {61694}, -- unstoppable (major resolve)
	[39205] = {61694}, -- unstoppable brute (major resolve)
	[39197] = {61694}, -- immovable (major resolve)

	-- Soul Magic
	[26768] = true,--{126890}, -- soul trap
	[40317] = true,--{126897}, -- consuming trap
	[40328] = true,--{126895}, -- soul splitting trap

	-- Fighters Guild
	[35750] = {61746}, -- trap beast
	[40372] = {61746}, -- lightweight beast trap
	[40382] = {61746}, -- barbed trap

	-- Mages Guild
	[28567] = true,--{126370}, -- entropy
	[40452] = true, --{126371}, -- structured entropy
	[40457] = true, --{126374}, -- degeneration
	[31632] = false, -- fire rune
	[40470] = false, -- volcanic rune
	[40465] = {40468}, -- scalding rune (dot)
	[40441] = {61694}, -- balance (major resolve)

	-- Psijic Order
	[103503] = {61746}, -- accelerate (minor force)
	[103706] = {61746}, -- channeled acceleration
	[103710] = {61746}, -- race against time

	-- Undaunted
	[39475] = 15, -- inner fire (taunt)
	[42056] = 15, -- inner rage (taunt)
	[42060] = 15, -- inner beast (taunt)
	[40195] = {61744}, -- camouflaged hunter (minor berserk)

	-- Alliance War
	[38566] = {61736}, -- rapid maneuver
	[40211] = {61736}, -- retreating maneuver
	[40215] = {61736}, -- charging maneuver
	[61503] = true, -- vigor
	[61505] = true, -- echoing vigor
	[61507] = true, -- resolving vigor

	-- Dragonknight
	[20492] = {61736}, -- fiery grip (minor expedition)
	[20496] = {61736}, -- unrelenting grip (minor expedition)
	[20499] = {61737}, -- empowering chains (empower)
	[20252] = {31898}, -- burning talons
	[20805] = {122658}, -- show seething fury on the molten whip icon
	[20657] = true, -- searing strike
	[20660] = true, -- burning embers
	[20668] = true, -- venomous claw
	[20917] = {31102}, -- fiery breath
	[20930] = {31104}, -- engulfing flames
	[20944] = {31103}, -- noxious breath
	[29004] = true, -- dragon blood
	[32722] = true, -- coagulating blood
	[32744] = true, -- green dragon blood
	[29043] = true, -- molten weapons
	[31874] = true, -- igneous weapons
	[31888] = true, -- molten armaments
	--[29071] = false, -- obsidian shield
	--[29224] = false, -- igneous shield
	--[32673] = false, -- fragmented shield
	[29032] = false, -- don't track stonefist
	[31816] = false, -- ignore stone giant
	[31816] = {134336}, -- track stagger instead
	[133027] = {134336}, -- track stagger instead

	-- Sorcerer
	[24595] = {61691}, -- dark deal
	[24589] = {61691}, -- dark conversion
	[108840] = {108842}, -- unstable familiar
	[23304] = {108842},
	[77182] = {77187}, -- volatile familiar
	[23316] = {77187},
	[77140] = {77354}, -- twilight tormentor
	[24636] = {77354},
	[46324] = {46327}, -- crystal fragment proc
	[114716] = {46327}, -- crystal fragment proc
	[130291] = {24165}, -- track Bound Armaments duration instead of its proc
	[23234] = {51392}, -- bolt escape fatigue
	[23236] = {51392}, -- streak fatigue
	[23277] = {51392}, -- ball of lightning fatigue

	-- Templar
	[21726] = {21728}, -- sun fire
	[21729] = {21731}, -- vampire's bane
	[21732] = {21734}, -- reflective light
	[26807] = true, -- radiant aura (magickasteal)
	[26188] = {95933}, -- spear shards
	[26858] = {95957}, -- luminous shards
	[26869] = {26880}, -- blazing spear
	[22265] = true, -- cleansing ritual
	[22259] = true, -- ritual of retribution
	[22262] = true, -- extended ritual

	-- Warden
	[85862] = {61706}, -- enchanted growth (minor intellect)
	[86023] = {101703}, -- swarm
	[86027] = {101904}, -- fetcher infection
	[86031] = {101944}, -- growing swarm
	[86037] = true, -- falcon's swiftness
	[86041] = true, -- deceptive predator
	[86045] = true, -- bird of prey
	[86122] = true, -- frost cloak
	[86126] = true, -- expansive frost cloak
	[86130] = true, -- ice fortress
	[86148] = 5, -- arctic wind
	[86152] = 5, -- polar wind
	[86156] = 5, -- arctic blast
	[86019] = 6, -- subterranean assault

	-- Nightblade
	[18342] = 10, -- teleport strike (minor vulnerability)
	[25484] = 10, -- ambush
	[25493] = 10, -- lotus fan
	[33375] = true,--{90587}, -- blur (major evasion)
	[35414] = true,--{90593}, -- mirage
	[35419] = true,--{90620}, -- phantasmal escape
	[61907] = false, -- grim focus proc
	[61930] = false, -- merciless resolve proc
	[61932] = false, -- relentless focus proc
	[25375] = true, -- shadow cloak
	[25380] = true, -- shadowy disguise
	[33211] = true, -- summon shade
	[35434] = true, -- dark shade
	[35441] = true,--{38528}, -- shadow image
	[33291] = {33292}, -- strife heal
	[34838] = {34841}, -- funnel health heal
	[34835] = {34836}, -- swallow soul heal
	[33326] = {33333}, -- cripple
	[36943] = {36947}, -- debilitate
	[36957] = {36960}, -- crippling grasp
	[33316] = {61687}, -- drain power (major sorcery)
	[36901] = {61687}, -- power extraction (major sorcery)
	[36891] = {61687}, -- sap essence (major sorcery)

	-- Necromancer
	[115315] = {115326}, -- life amid death
	[118017] = {118022}, -- renewing undeath
	[118809] = {118814}, -- enduring undeath
	[118352] = {61737}, -- Empowering grasp
}

-- stack id => ability id
AltActionBar.stackMap = {
	-- Grim Focus
	[61905] = 61902,
	[61920] = 61919,
	[61928] = 61927,
	-- Bound Armaments
	[130293] = 24165,
	-- Stone Giant
	[134336] = 134336,
	-- Seething Fury
	[122658] = 122658,
}