local LAM = LibAddonMenu2

function AltActionBar.BuildMenu(SV, defaults)

	local panel = {
		type = 'panel',
		name = 'Alternative Action Bar',
		displayName = 'Alternative Action Bar',
		author = '|c943810BulDeZir|r  (original from andy.s)',
		version = string.format('|c00FF00%s|r', AltActionBar.GetVersion()),
		registerForRefresh = true,
	}

	local options = {
		{
			type = "header",
			name = "|cFFFACDGeneral|r",
		},
		{
			type = "checkbox",
			name = "Show hotkeys",
			tooltip = "Show hotkeys under the action bar.",
			default = defaults.showHotkeys,
			getFunc = function() return SV.showHotkeys end,
			setFunc = function(value)
				SV.showHotkeys = value or false
				AltActionBar.ToggleHotkeys()
			end,
		},
		{
			type = "checkbox",
			name = "Show highlight",
			tooltip = "Active skills will be highlighted.",
			default = defaults.showHighlight,
			getFunc = function() return SV.showHighlight end,
			setFunc = function(value)
				SV.showHighlight = value or false
			end,
		},
		{
			type = "checkbox",
			name = "Show arrow",
			tooltip = "Show an arrow near the current bar.",
			default = defaults.showArrow,
			getFunc = function() return SV.showArrow end,
			setFunc = function(value)
				SV.showArrow = value or false
				AltAB_ActionBarArrow:SetHidden(not SV.showArrow)
			end,
		},
		{
			type = "colorpicker",
			name = "Arrow color",
			default = ZO_ColorDef:New(unpack(defaults.arrowColor)),
			getFunc = function() return unpack(SV.arrowColor) end,
			setFunc = function(r, g, b)
				SV.arrowColor = {r, g, b}
				AltAB_ActionBarArrow:SetColor(unpack(SV.arrowColor))
			end,
		},
		{
			type = "checkbox",
			name = "Debug mode",
			tooltip = "Display internal events in the game chat.",
			default = false,
			getFunc = function() return AltActionBar.IsDebugMode() end,
			setFunc = function(value)
				AltActionBar.SetDebugMode(value or false)
			end,
		},
	}

	local name = AltActionBar.GetName() .. 'Menu'
    LAM:RegisterAddonPanel(name, panel)
    LAM:RegisterOptionControls(name, options)

end