AltActionBar = {
	EVENT = {
		STYLE_READY = 'AltActionBarStyleReady',
		UI_READY = 'AltActionBarUiReady'
	}
}

local NAME = 'AltActionBar'
local VERSION = '1'

local defaultSettings = {
	showHotkeys = true,
	showHighlight = true,
	showArrow = false,
	arrowColor = {0, 1, 0},
}

local EM = EVENT_MANAGER
local SV
local strformat = string.format
local time = GetFrameTimeSeconds

local MIN_INDEX = 3 -- first ability index
local MAX_INDEX = 7 -- last ability index
local SLOT_INDEX_OFFSET = 20 -- offset for backbar abilities indices
local SLOT_COUNT = MAX_INDEX - MIN_INDEX + 1 -- total number of slots
local ACTION_BAR = ZO_ActionBar1
local abilityConfig = {} -- parsed FancyActionBar.abilityConfig
local stacks = {} -- ability id => current stack count
local buttons = {} -- backbar buttons
local overlays = {} -- buttons overlays
local effects = {} -- currently tracked effects

local currentHotbarCategory = GetActiveHotbarCategory()

local debug = false -- debug mode
local function dbg(msg, ...)
	if debug then d(strformat(msg, ...)) end
end

local GAMEPAD_CONSTANTS =
{
	abilitySlotWidth = 64,
    abilitySlotOffsetX = 10,
	buttonTextOffsetY = 60,
	actionBarOffset = -52,
	attributesOffset = -152,
}
local KEYBOARD_CONSTANTS =
{
	abilitySlotWidth = 50,
    abilitySlotOffsetX = 2,
	buttonTextOffsetY = 50,
	actionBarOffset = -22,
	attributesOffset = -112,
}

function AltActionBar.GetName()
	return NAME
end

function AltActionBar.GetVersion()
	return VERSION
end

-- Parse ActionBar.abilityConfig for faster access.
function AltActionBar.BuildAbilityConfig()
	for id, cfg in pairs(AltActionBar.abilityConfig) do
		if type(cfg) == 'table' then
			abilityConfig[id] = {cfg[1] or id, cfg[2]}
		elseif cfg then
			abilityConfig[id] = {id, type(cfg) == 'number' and cfg or nil, true}
		elseif cfg == false then
			abilityConfig[id] = false
		else
			abilityConfig[id] = nil
		end
	end
end

-- Get ActionButton by index.
function AltActionBar.GetActionButton(index)
	if index > SLOT_INDEX_OFFSET then
		return buttons[index]
	else
		return ZO_ActionBar_GetButton(index)
	end
end

-- Show/hide hotkeys.
function AltActionBar.ToggleHotkeys()
	for i = MIN_INDEX, MAX_INDEX do
		ZO_ActionBar_GetButton(i).buttonText:SetHidden(not SV.showHotkeys)
	end
end

function AltActionBar.IsDebugMode()
	return debug
end

function AltActionBar.SetDebugMode(mode)
	assert(type(mode) == 'boolean', 'Debug mode must be boolean.')
	debug = mode
end

-- Move action bar and attributes up a bit.
local function AdjustControlsPositions()
	local style = IsInGamepadPreferredMode() and GAMEPAD_CONSTANTS or KEYBOARD_CONSTANTS
	local anchor = ZO_Anchor:New()
	anchor:SetFromControlAnchor(ACTION_BAR)
	anchor:SetOffsets(nil, style.actionBarOffset)
	anchor:Set(ACTION_BAR)

	anchor:SetFromControlAnchor(ZO_PlayerAttribute)
	anchor:SetOffsets(nil, style.attributesOffset)
	anchor:Set(ZO_PlayerAttribute)
end

-- Backbar control initialized.
function AltActionBar.OnActionBarInitialized(control)
	-- Set active bar as a parent to make inactive bar show/hide automatically.
	control:SetParent(ACTION_BAR)

	-- Need to adjust it here instead of in ApplyStyle(), otherwise it won't properly work with Azurah.
	AdjustControlsPositions()

	-- Create inactive bar buttons.
    for i = MIN_INDEX + SLOT_INDEX_OFFSET, MAX_INDEX + SLOT_INDEX_OFFSET do
		local button = ActionButton:New(i, ACTION_BUTTON_TYPE_VISIBLE, control, 'ZO_ActionButton')
		button:SetShowBindingText(false)
		button.icon:SetHidden(true)
		button:SetupBounceAnimation()
		buttons[i] = button
    end

	CALLBACK_MANAGER:FireCallbacks(AltActionBar.EVENT.UI_READY, control)
end

-- Create button overlay.
function CreateOverlay(index)
	local template = ZO_GetPlatformTemplate('AltAB_ActionButtonOverlay')
	local overlay = overlays[index]
	if overlay then
		ApplyTemplateToControl(overlay, template)
		overlay:ClearAnchors()
	else
		overlay = CreateControlFromVirtual('ActionButtonOverlay', ACTION_BAR, template, index)
		overlays[index] = overlay
	end
	return overlay
end

-- Update overlay controls.
function UpdateOverlay(index)
	local overlay = overlays[index]
	if overlay then
		local effect = overlay.effect
		local durationControl = overlay:GetNamedChild('Duration')
		local stacksControl = overlay:GetNamedChild('Stacks')
		local bgControl = overlay:GetNamedChild('BG')
		if effect then
			-- duration
			local duration = effect.endTime - time()
			if duration > -3 then
				if duration > 0 then
					durationControl:SetColor(1, 1, 1)
					bgControl:SetHidden(not SV.showHighlight)
				else
					durationControl:SetColor(1, 1, 0)
					bgControl:SetHidden(true)
				end
				durationControl:SetText(zo_max(0, zo_ceil(duration)))
			else
				bgControl:SetHidden(true)
				durationControl:SetText('')
			end
			-- stacks
			if stacks[effect.id] and stacks[effect.id] > 0 then
				stacksControl:SetText(stacks[effect.id])
			else
				stacksControl:SetText('')
			end
		else
			bgControl:SetHidden(true)
			durationControl:SetText('')
		end
	end
end

-- Remove effect from overlay index.
function UnslotEffect(index)
	local overlay = overlays[index]
	if overlay then
		local effect = overlay.effect
		if effect then
			if index < SLOT_INDEX_OFFSET then effect.slot1 = nil else effect.slot2 = nil end
			overlay.effect = nil
		end
	end
end

-- Assign effect to overlay index.
function SlotEffect(index, abilityId)
	if not abilityId or abilityId == 0 or GetAbilityCastInfo(abilityId) then
		UnslotEffect(index)
	else
		local cfg = abilityConfig[abilityId]
		local effectId, duration, simple
		if cfg == false then
			return
		elseif cfg then
			effectId = cfg[1] or abilityId
			simple = cfg[3] or false
			duration = cfg[2] or simple and GetAbilityDuration(abilityId) / 1000
		else
			effectId = abilityId
			duration = GetAbilityDuration(abilityId) / 1000
			simple = false
		end
		local effect = effects[effectId]
		if not effect then
			effect = {id = effectId, simple = simple, endTime = 0}
			effects[effectId] = effect
		end
		effect.duration = duration and duration > 0 and duration or nil
		if index < SLOT_INDEX_OFFSET then effect.slot1 = index else effect.slot2 = index end
		-- Assign effect to overlay.
		local overlay = overlays[index]
		if overlay then overlay.effect = effect end
		return effect
	end
end

-- Slot effects for primary and backup bars.
function SlotEffects()
	if currentHotbarCategory == HOTBAR_CATEGORY_PRIMARY or currentHotbarCategory == HOTBAR_CATEGORY_BACKUP then
		for i = MIN_INDEX, MAX_INDEX do
			SlotEffect(i, GetSlotBoundId(i, HOTBAR_CATEGORY_PRIMARY))
			SlotEffect(i + SLOT_INDEX_OFFSET, GetSlotBoundId(i, HOTBAR_CATEGORY_BACKUP))
		end
	else
		-- Unslot all effects, if we are on a special bar.
		for i = MIN_INDEX, MAX_INDEX do
			UnslotEffect(i)
			UnslotEffect(i + SLOT_INDEX_OFFSET)
		end
	end
end

-- Update overlays linked to the effect.
function UpdateEffect(effect)
	if effect then
		if effect.slot1 then UpdateOverlay(effect.slot1) end
		if effect.slot2 then UpdateOverlay(effect.slot2) end
	end
end

-- Apply style to action bars depending on keyboard/gamepad mode.
local function ApplyStyle()

	local style = IsInGamepadPreferredMode() and GAMEPAD_CONSTANTS or KEYBOARD_CONSTANTS

	-- Most alignments are relative to weapon swap button.
	local weaponSwapControl = ACTION_BAR:GetNamedChild('WeaponSwap')

	-- Hide default background.
	ACTION_BAR:GetNamedChild('KeybindBG'):SetHidden(true)

	-- Hide weapon swap button.
	weaponSwapControl:SetAlpha(0)

	-- Arrow style.
	AltAB_ActionBarArrow:SetHidden(not SV.showArrow)
	AltAB_ActionBarArrow:SetColor(unpack(SV.arrowColor))

	-- Reposition ultimate slot.
	ActionButton8:ClearAnchors()
	ActionButton8:SetAnchor(LEFT, weaponSwapControl, RIGHT, SLOT_COUNT * (style.abilitySlotWidth + 2 * style.abilitySlotOffsetX), 0)

	-- Set positions for buttons and overlays.
	local lastButton
	local buttonTemplate = ZO_GetPlatformTemplate('ZO_ActionButton')
	for i = MIN_INDEX, MAX_INDEX do
		local button = ZO_ActionBar_GetButton(i)
		local overlayOffsetX = (i - MIN_INDEX) * (style.abilitySlotWidth + style.abilitySlotOffsetX)

		-- Hotkey position.
		button.buttonText:ClearAnchors()
		button.buttonText:SetAnchor(TOP, weaponSwapControl, RIGHT, overlayOffsetX + style.abilitySlotWidth / 2, style.buttonTextOffsetY)
		button.buttonText:SetHidden(not SV.showHotkeys)

		-- Main button overlay.
		local overlay = CreateOverlay(i)
		overlay:SetAnchor(BOTTOMLEFT, weaponSwapControl, RIGHT, overlayOffsetX, -4)

		-- Backbar button style and position.
		button = buttons[i + SLOT_INDEX_OFFSET]
		button:ApplyStyle(buttonTemplate)
		button.icon:SetDesaturation(0.9)
		button.icon:SetAlpha(0.3)
		if lastButton then
			button:ApplyAnchor(lastButton.slot, style.abilitySlotOffsetX)
		end
		lastButton = button

		-- Back button overlay.
		overlay = CreateOverlay(i + SLOT_INDEX_OFFSET)
		overlay:SetAnchor(TOPLEFT, weaponSwapControl, RIGHT, overlayOffsetX, 0)
	end

	CALLBACK_MANAGER:FireCallbacks(AltActionBar.EVENT.STYLE_READY, weaponSwapControl, style)
end

-- Refresh action bars positions.
local function SwapControls()

	-- Set new anchors for the first buttons.
	local weaponSwapControl = ACTION_BAR:GetNamedChild('WeaponSwap')
	ActionButton3:ClearAnchors()
	ActionButton23:ClearAnchors()
	if currentHotbarCategory == HOTBAR_CATEGORY_BACKUP then
		ActionButton3:SetAnchor(TOPLEFT, weaponSwapControl, RIGHT, 0, 0)
		ActionButton23:SetAnchor(BOTTOMLEFT, weaponSwapControl, RIGHT, 0, -4)
	else
		ActionButton3:SetAnchor(BOTTOMLEFT, weaponSwapControl, RIGHT, 0, -4)
		ActionButton23:SetAnchor(TOPLEFT, weaponSwapControl, RIGHT, 0, 0)
	end

	-- Update icons for inactive bar.
	for i = MIN_INDEX, MAX_INDEX do
		local btnBackSlotId = GetSlotBoundId(i, currentHotbarCategory == HOTBAR_CATEGORY_BACKUP and HOTBAR_CATEGORY_PRIMARY or HOTBAR_CATEGORY_BACKUP)
		local btnBack = buttons[i + SLOT_INDEX_OFFSET]
		if btnBackSlotId > 0 then
			btnBack.icon:SetTexture(GetAbilityIcon(btnBackSlotId))
			btnBack.icon:SetHidden(false)
		else
			btnBack.icon:SetHidden(true)
		end
		-- Need to update main buttons manually, because by default it is done when animation ends.
		local btnMain = ZO_ActionBar_GetButton(i)
		btnMain:HandleSlotChanged()
	end

	-- Unslot effects from the main bar if it's currently a special bar.
	if currentHotbarCategory ~= HOTBAR_CATEGORY_PRIMARY and currentHotbarCategory ~= HOTBAR_CATEGORY_BACKUP then
		for i = MIN_INDEX, MAX_INDEX do
			UnslotEffect(i)
		end
	end
end

local function Initialize()

	SV = ZO_SavedVars:NewAccountWide('AltActionBarSavedVariables', 1, nil, defaultSettings)

	AltActionBar.BuildMenu(SV, defaultSettings)
	AltActionBar.BuildAbilityConfig()

	-- Can use abilities while map is open, when cursor is active, etc.
	ZO_ActionBar_CanUseActionSlots = function() return true end

	-- Slot ability changed, e.g. summoned a pet, procced crystal, etc.
	local function OnSlotChanged(_, n)
		local btn = ZO_ActionBar_GetButton(n)
		if btn then
			btn:HandleSlotChanged()
		end
	end

	-- Button (usable) state changed.
	local function OnSlotStateChanged(_, n)
		local btn = ZO_ActionBar_GetButton(n)
		if btn then btn:UpdateState() end
	end

	-- Any skill swapped. Setup buttons and slot effects.
	local function OnAllHotbarsUpdated()
		for i = MIN_INDEX, MAX_INDEX do
			local button = ZO_ActionBar_GetButton(i)
			if button then
				button.hotbarSwapAnimation = nil -- delete default animation
				button.noUpdates = true -- disable animation updates
				button:HandleSlotChanged() -- update slot manually
			end
		end
		SlotEffects()
	end

	local function OnActiveWeaponPairChanged()
		currentHotbarCategory = GetActiveHotbarCategory()
		SwapControls()
	end

	local function OnAbilityUsed(_, n)
		if (currentHotbarCategory == HOTBAR_CATEGORY_PRIMARY or currentHotbarCategory == HOTBAR_CATEGORY_BACKUP) and n >= MIN_INDEX and n <= MAX_INDEX then
			local index = currentHotbarCategory == HOTBAR_CATEGORY_BACKUP and n + SLOT_INDEX_OFFSET or n
			local id = GetSlotBoundId(n, currentHotbarCategory)

			local duration = GetAbilityDuration(id) / 1000
			local effect = SlotEffect(index, id)
			if effect then
				if id == 114716 then
					-- instantly remove crystal fragments proc duration
					effect.endTime = 0
				elseif effect.simple then
					effect.endTime = time() + effect.duration
				end
			end
			dbg('[ActionButton%d] #%d: %0.1fs', index, id, duration)
		end
	end

	local function OnEffectChanged(_, change, _, effectName, unitTag, beginTime, endTime, stackCount, _, _, _, _, _, _, _, abilityId)
		if change == EFFECT_RESULT_GAINED or change == EFFECT_RESULT_UPDATED then
			local effect = effects[abilityId]
			if effect then
				local t = time()
				if effect.duration then
					endTime = t + effect.duration
				end
				-- Ignore abilities which will end in less than 2s or longer than 100s.
				if endTime > t + 2 and endTime < t + 100 then
					effect.endTime = endTime
					UpdateEffect(effect)
				else
					effect.endTime = 0
				end
			end
			dbg('%s #%d - %d (%s)', change == EFFECT_RESULT_GAINED and '+' or '*', abilityId, endTime - beginTime, effectName)
		end
	end

	-- Update overlays.
	local function Update()
		for i, overlay in pairs(overlays) do
			UpdateOverlay(i)
		end
	end

	-- Abilities stacks.
	function OnStackChanged(_, changeType, _, _, _, _, _, stackCount, _, _, _, _, _, _, _, abilityId)
		stacks[AltActionBar.stackMap[abilityId]] = changeType == EFFECT_RESULT_FADED and 0 or stackCount
	end

	for abilityId in pairs(AltActionBar.stackMap) do
		EM:RegisterForEvent(NAME .. abilityId, EVENT_EFFECT_CHANGED, OnStackChanged)
		EM:AddFilterForEvent(NAME .. abilityId, EVENT_EFFECT_CHANGED, REGISTER_FILTER_ABILITY_ID, abilityId)
		EM:AddFilterForEvent(NAME .. abilityId, EVENT_EFFECT_CHANGED, REGISTER_FILTER_SOURCE_COMBAT_UNIT_TYPE, COMBAT_UNIT_TYPE_PLAYER)
	end

	EM:RegisterForEvent(NAME, EVENT_ACTION_SLOT_UPDATED, OnSlotChanged)
	EM:RegisterForEvent(NAME, EVENT_ACTION_SLOT_STATE_UPDATED, OnSlotStateChanged)
	EM:RegisterForEvent(NAME, EVENT_ACTION_SLOTS_ALL_HOTBARS_UPDATED, OnAllHotbarsUpdated)
	EM:RegisterForEvent(NAME, EVENT_ACTION_SLOT_ABILITY_USED, OnAbilityUsed)
	EM:RegisterForEvent(NAME, EVENT_ACTIVE_WEAPON_PAIR_CHANGED, OnActiveWeaponPairChanged)
	EM:RegisterForEvent(NAME, EVENT_GAMEPAD_PREFERRED_MODE_CHANGED, function() ApplyStyle(); SwapControls(); AdjustControlsPositions() end)
	EM:RegisterForEvent(NAME, EVENT_PLAYER_ACTIVATED, function()
		currentHotbarCategory = GetActiveHotbarCategory()
		ApplyStyle()
		OnAllHotbarsUpdated()
		SwapControls()
		EM:UnregisterForUpdate(NAME .. 'Update')
		EM:RegisterForUpdate(NAME .. 'Update', 200, Update)
		EM:UnregisterForEvent(NAME, EVENT_PLAYER_ACTIVATED)
	end)

	EM:RegisterForEvent(NAME, EVENT_EFFECT_CHANGED, OnEffectChanged)
	EM:AddFilterForEvent(NAME, EVENT_EFFECT_CHANGED, REGISTER_FILTER_SOURCE_COMBAT_UNIT_TYPE, COMBAT_UNIT_TYPE_PLAYER)

end

local function OnAddOnLoaded(event, addonName)
	if addonName == NAME then
		EM:UnregisterForEvent(NAME, EVENT_ADD_ON_LOADED)
		Initialize()
	end
end

EM:RegisterForEvent(NAME, EVENT_ADD_ON_LOADED, OnAddOnLoaded)